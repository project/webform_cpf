# webform CPF

INTRODUCTION
------------

webform cpf module provides a webform CPF field to be used at webform forms

REQUIREMENTS
------------

This module requires the following modules:

 - [webform](https://www.drupal.org/project/webform)
 - [cpf](https://www.drupal.org/project/cpf)
 - [markdown](https://www.drupal.org/project/markdown)

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
You should use composer or download the required modules manually.

CONFIGURATION
-------------

This module do not require any configuration. It just make
available a field of type cpf in webform build tab. 

MAINTAINERS
-----------

Current maintainers:

 - [Thiago Gomes Veríssimo](https://www.drupal.org/u/thiagogomesverissimo)
