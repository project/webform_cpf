<?php

namespace Drupal\Tests\webform_cpf\Functional;

use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Tests for webform cpf.
 *
 * @group Webform
 */
class WebformCpfTest extends WebformBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['webform_cpf'];

  /**
   * Tests webform cpf.
   */
  public function testWebformCpf() {
    $webform = Webform::load('webform_cpf');

    // Check form element rendering.
    $this->drupalGet('/webform/webform_cpf');
    // NOTE:
    // This is a very lazy but easy way to check that the element is rendering
    // as expected.
    $this->assertRaw('<div class="js-form-item form-item js-form-type-webform-cpf form-type-webform-cpf js-form-item-webform-cpf form-item-webform-cpf">');
    $this->assertRaw('<label for="edit-webform-cpf">Webform Example Element</label>');
    $this->assertRaw('<input data-drupal-selector="edit-webform-cpf" type="text" id="edit-webform-cpf" name="webform_cpf" value="" size="60" class="form-text webform-cpf" />');

    // Check webform element submission.
    $edit = [
      'webform_cpf' => '{Test}',
      'webform_cpf_multiple[items][0][_item_]' => '{Test 01}',
    ];
    $sid = $this->postSubmission($webform, $edit);
    $webform_submission = WebformSubmission::load($sid);
    $this->assertEqual($webform_submission->getElementData('webform_cpf'), '{Test}');
    $this->assertEqual($webform_submission->getElementData('webform_cpf_multiple'), ['{Test 01}']);
  }

}
