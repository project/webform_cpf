<?php

namespace Drupal\webform_cpf\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Drupal\webform\WebformLibrariesManagerInterface;
use Drupal\webform\Plugin\WebformElementManagerInterface;

/**
 * Provides a 'webform_cpf' element.
 *
 * @WebformElement(
 *   id = "webform_cpf",
 *   label = @Translation("Webform cpf"),
 *   description = @Translation("Provides a webform cpf element."),
 *   category = @Translation("cpf elements"),
 * )
 *
 * @see \Drupal\webform_cpf\Element\WebformCpf
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformCpf extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return parent::getDefaultProperties() + [
      'multiple' => '',
      'size' => '',
      'minlength' => '',
      'maxlength' => '',
      'placeholder' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(array &$element, WebformSubmissionInterface $webform_submission) {
    $data = $webform_submission->getData();
    $data[$element["#webform_key"]] = preg_replace( '/[^0-9]/is', '', $data[$element["#webform_key"]]);
    $webform_submission->setData($data);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    return $form;
  }
}
