<?php

namespace Drupal\webform_cpf\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_cpf'.
 *
 * @FormElement("webform_cpf")
 */
class WebformCpf extends FormElement  {


  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#process' => [
        [$class, 'processWebformCpf'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$this, 'validateWebformCpf'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformCpf'],
      ],
      '#theme' => 'input__webform_cpf',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'webform_cpf' element.
   */
  public static function processWebformCpf(&$element, FormStateInterface $form_state, &$complete_form) {
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_cpf'.
   */
  public function validateWebformCpf(&$element, FormStateInterface $form_state, &$complete_form) {

    if (!$this->validaCPF($element["#value"])) {
      $form_state->setErrorByName($element["#webform_key"],
        t('The CPF number %cpf is not valid', ['%cpf' => $element["#value"]]));
    }
    
  }

  /**
   * Prepares a #type 'webform-cpf' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformCpf(array $element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
    ]);
    static::setAttributes($element, ['form-text', 'webform-cpf']);
    return $element;
  }

  private function validaCPF($cpf) {
    $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
    if (strlen($cpf) != 11) {
        return false;
    }
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf[$c] * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf[$c] != $d) {
            return false;
        }
    }
    return true;
  }

}
